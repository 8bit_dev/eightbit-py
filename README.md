Eightbit Python Client
====================

This API interface provides a simple access to our strong AI tech. 

As you get an APIKEY from our web interface, you can directly s use our API.
Constructor takes an APIKEY and you call the API for web images or local files. 
You can also define an environment variable EIGHTBIT_APIKEY so that you can 
call this interface.

Out API has some contraints for the given images. If you like to automatically correct local
images before sending to our servers, you need to install PIL library beforehand or you need 
to handle it by yourself.

Installation
---------------------

pip install git+http://bitbucket.org/8bit_dev/eightbit-py.git

export EIGHTBIT_APIKEY=<your-apikey>

Usage
---------------------

A complete example of using 8bit Python client is given inside 'sample_call.py'. 
It is as simple as that.

Please contact us for anything - 8bit@8bitvision.com