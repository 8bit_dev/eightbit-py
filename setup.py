try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    description='8Bit API Python Client',
    author='8Bit',
    url='https://github.com/eightbit/eightbit_py',
    author_email='8bit@8bitvision.com',
    version='0.0.2',
    install_requires=['six'],
    namespace_packages=['eightbit'],
    packages=['eightbit.client'],
    scripts=[],
    name='eightbit',
)
