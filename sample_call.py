#!/usr/bin/env python

import os
import sys
import time
from eightbit.client import EightbitApi


def main(argv):
  imageurl = 'http://data.whicdn.com/images/25647064/desktopwallpapers.org.ua-9381_large.jpg'
  imagepath = 'sample_data/husky.jpg'

  api = EightbitApi(apikey = 'your APIKEY')
  
  # tag an image from a web url
  res = api.tag_url(imageurl)
  print(res)

  print("waiting for 5secs bandwidth timeout..")
  time.sleep(5)
  
  # tag an image from local path
  res = api.tag_file(imagepath)
  print(res)


if __name__ == '__main__':
  main(sys.argv)
